import { createStackNavigator, createAppContainer } from "react-navigation";
import InicioScreen from './src/InicioScreen'
import PerfilScreen from './src/PerfilScreen'

const AppNavigator = createStackNavigator(
	{
		Inicio: InicioScreen,
		Perfil: PerfilScreen
	},
	{
		initialRouteName: "Inicio"
	}
);

export default createAppContainer(AppNavigator);