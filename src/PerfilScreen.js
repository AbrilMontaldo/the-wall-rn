import React from "react";
import { ScrollView, View } from "react-native";
import { Text, Card, Divider } from 'react-native-elements'
import Moment from 'moment';
Moment.locale('es');

export default class PerfilScreen extends React.Component {
	static navigationOptions = {
		title: 'THE WALL',
		headerStyle: {
			backgroundColor: '#f4511e',
		},
		headerTintColor: '#fff',
		headerTitleStyle: {
			fontWeight: 'bold',
		},
	};
  
	constructor(props){
		super(props);
		this.state = {
			user: {},
			userId: props.navigation.state.params.userId
		}
	}

	componentWillMount(){
		fetch('http://192.168.0.16:8000/api/usuarios/'+this.state.userId)
			.then(res => res.json())
			.then(data => this.setState({user:data}));
	}

	render() {
		// let mensajes = this.state.user.mensajes ?
		console.log(this.state.user);

		return (
			<ScrollView>
				<Card>
					<Text h5>{this.state.user.nombre} {this.state.user.apellido}</Text>
					<Text style={{margin: 15}} h4>{this.state.user.username}</Text>
					<Divider style={{ backgroundColor: 'lightgrey', height:10}} />
					{this.state.user.mensajes ? 
						this.state.user.mensajes.map(mensaje => (
							<>
								<View style={{margin: 15}} key={mensaje.id}>
									<Text style={{color:'#f4511e'}}>{this.state.user.nombre + ' ' + this.state.user.apellido}</Text>
									<Text>{Moment(mensaje.created_at).format('d/m/Y HH:mm')}</Text>
									<Text>{mensaje.contenido}</Text>
								</View>
								<Divider style={{ backgroundColor: 'lightgrey' , height:2}} />
							</>
						))
						: null
					}
				</Card>
			</ScrollView>
		);
	}
}