import React from "react";
import { ScrollView, Text } from "react-native";
import { Card, Button  } from 'react-native-elements'
import Moment from 'moment';
Moment.locale('es');

export default class InicioScreen extends React.Component {
	static navigationOptions = {
		title: 'THE WALL',
		headerStyle: {
			backgroundColor: '#f4511e',
		},
		headerTintColor: '#fff',
		headerTitleStyle: {
			fontWeight: 'bold',
		},
	};

	constructor(props){
		super(props);
		this.state = {
			mensajes: []
		}
	}
	componentDidMount(){
		fetch('http://192.168.0.16:8000/api/mensajes')
			.then(res => res.json())
			.then(data => this.setState({mensajes:data}));
	}

    render() {
		return (
			<ScrollView>
				{this.state.mensajes.map(mensaje => (
					<Card key={mensaje.id}>
						<Button
							titleStyle= {{color: '#f4511e'}}
							title={mensaje.user.nombre + ' ' + mensaje.user.apellido}
							type="clear"
							onPress={() => this.props.navigation.navigate('Perfil', {userId: mensaje.user.id})}
						/>
						<Text>{Moment(mensaje.created_at).format('d/m/Y HH:mm')}</Text>
						<Text>{mensaje.contenido}</Text>
					</Card>
				))}
			</ScrollView>
		);
    }
}